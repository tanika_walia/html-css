/**
 * Created by umesh on 6/1/15.
 */

var app= angular.module('jugnooWebsite', ['ngRoute']);


app.config(function ($routeProvider) {
    $routeProvider
        .when('/',{
            controller: 'websiteController',
            templateUrl: 'new-website/views/home.html'
        })
        .when('/home',{
            controller: 'websiteController',
            templateUrl: 'new-website/views/home.html'
        })
        .when('/about',{
            controller: 'websiteController',
            templateUrl: 'new-website/views/about.html'
        })
        .when('/career',{
            controller: 'websiteController',
            templateUrl: 'new-website/views/career.html'
        })
        .when('/pressmedia',{
            controller: 'websiteController',
            templateUrl: 'new-website/views/pressmedia.html'
        })
        .when('/investors',{
            controller: 'websiteController',
            templateUrl: 'new-website/views/investors.html'
        })
        .when('/helpcenter',{
            controller: 'websiteController',
            templateUrl: 'new-website/views/helpcenter.html'
        })
        .when('/contactus',{
            controller: 'websiteController',
            templateUrl: 'new-website/views/contactus.html'
        })
        .when('/terms',{
            controller: 'websiteController',
            templateUrl: 'new-website/views/terms.html'
        })
        .when('/privacy',{
            controller: 'websiteController',
            templateUrl: 'new-website/views/privacy.html'
        })
        .when('/autos',{
            controller: 'websiteController',
            templateUrl: 'new-website/views/autos.html'
        })
        .when('/meals',{
            controller: 'websiteController',
            templateUrl: 'new-website/views/meals.html'
        })
        .when('/fatafat',{
            controller: 'websiteController',
            templateUrl: 'new-website/views/fatafat.html'
        })
        .when('/otp',{
            controller: 'websiteController',
            templateUrl: 'new-website/views/otp.html'
        })
        .when('/user_profile',{
            controller: 'websiteController',
            templateUrl: 'new-website/views/user_profile.html'
        })
        .when('/login',{
            controller: 'websiteController',
            templateUrl: 'new-website/views/login.html'
        })
});



if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
    var msViewportStyle = document.createElement('style');
    msViewportStyle.appendChild(
        document.createTextNode(
            '@-ms-viewport{width:auto!important}'
        )
    );
    document.querySelector('head').appendChild(msViewportStyle)
}


// Cookies
function createCookie(name,value,days) {
    var expires;
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        expires = "; expires="+date.toUTCString();
    }
    else expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
    return null;
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0)
            return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}