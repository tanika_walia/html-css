/**
 * Created by umesh on 6/1/15.
 */


app.controller("websiteController", ['$scope','$location','$route', '$routeParams', function ($scope,$route, $routeParams, $location) {
    $scope.$route = $route;
    $scope.$location = $location;
    $scope.$routeParams = $routeParams;

    console.log($route.$$path);
}]);

app.controller("pressMediaController", ['$scope', function ($scope) {
    var imgsrc = 'new-website/assets/pressmedia/';
    $scope.pressmediaObject = [
        {
            link:'http://techcrunch.com/2015/06/05/jugnoo/',
            desc:'On-Demand App Jugnoo Gets $5M To Bring India’s Auto Rickshaws Online ',
            img: imgsrc + 'tc.png',
            date:'05-Jun-2015'
        },
        {
            link:'http://www.business-standard.com/article/companies/autorickshaw-app-now-gets-cabs-too-115080300002_1.html',
            desc:'Autorickshaw app, now gets cabs, too',
            img: imgsrc + 'business_standard.jpeg',
            date:'3-Aug-2015'
        },
        {
            link:'http://yourstory.com/2015/07/jugnoo-acquires-bookmycab-hyperlocal/',
            desc:'Paytm-backed hyperlocal platform Jugnoo acquires taxi aggregator Bookmycab, to raise $20M Series B soon',
            img: imgsrc + 'ys.png',
            date:'28-July-2015'
        },
        {
            link:'http://yourstory.com/2015/06/jugnoo-chandigarh/',
            desc:'How Jugnoo scaled up to 90K transactions in 7 months and why it said no to Flipkart and Naspers investment ',
            img: imgsrc + 'ys.png',
            date:'11-Jun-2015'
        },
        {
            link:'http://inc42.com/startups/jugnoo/',
            desc:'Exclusive: Jugnoo Is Using Autos As A Logistics Engine To Provide Everything On-Demand, Gets $1 Mn ',
            img: imgsrc + 'inc.png',
            date:'23-Apr-2015'
        },
        {
            link:'http://www.indianretailer.com/article/whats-hot/investments/Jugnoo-bags-5-mn-from-Snow-Leopard-Ventures-Paytm.a3509/',
            desc:'Jugnoo bags $5 mn from Snow Leopard Ventures, Paytm',
            img: imgsrc + 'ir.png',
            date:'06-Jun-2015'
        },
        {
            link:'http://economictimes.indiatimes.com/small-biz/startups/jugnoo-raises-5-million-from-snow-leopard-ventures-paytm/articleshow/47552996.cms',
            desc:'Jugnoo raises $5 million from Snow Leopard Ventures, Paytm ',
            img: imgsrc + 'et.png',
            date:'05-Jun-2015'
        },
        {
            link:'http://trak.in/tags/business/2014/11/29/jugnoo-mobile-app-on-demand-auto-service/',
            desc:'Jugnoo, A Mobile App For On Demand Auto Rickshaw Service',
            img: imgsrc + 'trakin.jpg',
            date:'29-Nov-2014'
        },
        {
            link:'http://www.iamwire.com/2015/05/jugnoo-indian-startup-enabling-hyper-local-delivery-transportation-autos/115562',
            desc:'Jugnoo: An Indian Startup Enabling Hyper Local Delivery and Transportation Using Autos! ',
            img: imgsrc + 'iamwire.png',
            date:'1-May-2015'
        },
        {
            link:'http://www.dealcurry.com/20150429-On-Demand-Logistics-Startup-Jugnoo-Secures-Seed-Funding.htm',
            desc:'On Demand Logistics Startup Jugnoo Secures Seed Funding ',
            img: imgsrc + 'deal.png',
            date:'29-April-2015'
        },
        {
            link:'http://www.hindustantimes.com/chandigarh/chandigarh-s-online-entrepreneurs-the-clicks-that-count/article1-1363981.aspx',
            desc:"Chandigarh's online entrepreneurs: The clicks that count ",
            img: imgsrc + 'ht.jpeg',
            date:'29-Jun-2015'
        },
        {
            link:'http://yourstory.com/2015/07/chandigarh-accelerator-startup-sacc/',
            desc:"Chandigarh-based startup accelerator helping startups without taking any equity ",
            img: imgsrc + 'ys.png',
            date:'10-July-2015'
        },
        {
            link:'http://techcircle.vccircle.com/2015/06/24/a-look-at-the-four-promising-startups-showcased-at-techcircle-mobile-forum-2015/',
            desc:'A look at the four promising startups showcased at Techcircle Mobile Forum 2015 ',
            img: imgsrc + 'tech-circle.png',
            date:'24-Jun-2015'
        },
        {
            link:'http://www.idgconnect.com/abstract/10213/jugnoo-indian-auto-rickshaw-network',
            desc:'Jugnoo & the Indian auto rickshaw network',
            img: imgsrc + 'IDG_Connect.jpg',
            date:'22-July-2015'
        },
        {
            link:'http://www.business-standard.com/article/companies/onion-crisis-makes-cheap-marketing-tool-for-chandigarh-startup-115082400642_1.html',
            desc:'Onion crisis makes cheap marketing tool for Chandigarh startup',
            img: imgsrc + 'business_standard.jpeg',
            date:'24-Aug-2015'
        }
    ];
}]);

app.controller('investorSectionController',['$scope',function($scope){
    $scope.angelInvestorList = [
        {
            image: "new-website/assets/investors/vikas_taneja.jpeg",
            name: "VIKAS TANEJA",
            description: "Angel investor in RippleLabs, InboxVudu ,500 Startups etc.,Sr Partner BCG,Booth MBA at Stanford CS"
        },
        {
            image: "new-website/assets/investors/shalin_shah.jpeg",
            name: "SHALIN SHAH",
            description: "Angel investor,Vice President at Piramal Enterprises/IndUS Growth Partners"
        },
        {
            image: "new-website/assets/investors/jagdish_amin.jpeg",
            name: "Jagdish Amin",
            description: "Global experience. Part of 2 start ups. Worked in the Enterprise and eCommerce space"
        },
        {
            image: "new-website/assets/investors/jai_farswani.jpeg",
            name: "Jai Farswani",
            description: "Entrepreneur & Angel Investor. Co-Founder & COO of HouseStay .Strategic Planning at International Lease Finance Corporation (now AerCap)."
        },
        {
            image: "new-website/assets/investors/placeholder.png",
            name: "Justin Weil",
            description: "Investor in Facebook, Yandex"
        }

    ]
}]);

app.controller("LogoutController", ['$scope', '$location', function ($scope, $location) {
    $scope.logout = function () {
        createCookie("user_name","",-1);
        createCookie("phone_no","",-1);
        createCookie("referral_code","",-1);
        createCookie("current_balance","",-1);
    }
}]);
app.controller("userProfileController", ['$scope', '$location', function ($scope, $location) {

    if(!readCookie("user_name")){
        $location.url('/login');
    }

    $scope.name = readCookie("user_name");
    $scope.phone_no = readCookie("phone_no");
    $scope.referral_code = readCookie("referral_code");
    $scope.balance = readCookie("current_balance");

}]);

app.controller("otpController", ['$scope', '$window', function ($scope, $window) {

    $scope.cookiePhone = readCookie("otp_phone_no");
    $scope.otpMessage = '';

    $sscope.otpClicked = function () {

        var emailText = readCookie("otp_email");
        var phoneNumberText = readCookie("otp_phone_no");
        var passwordText = readCookie("otp_password");
        var referralText = "";
        var otpString = $scope.otp;

        if (readCookie("otp_referral_code") !== "" && readCookie("otp_referral_code") !== null) {
            referralText = readCookie("otp_referral_code");
        }
        if (emailText.length !== 0 && phoneNumberText.length !== 0 && passwordText.length !== 0 && otpString.length !== 0) {

            serverCallToSubmitOTP(emailText, phoneNumberText, passwordText, referralText, otpString);

            console.log(otpString + ' And ' + emailText + ' And ' + phoneNumberText + ' And ' + passwordText + ' And ' + ' And ' + referralText);
        }

        else {
            if (otpString.length == 0) {
                $scope.otpMessage = 'Please fill the OTP';
            }
        }
    };

    function serverCallToSubmitOTP(emailText, phoneNumber, passwordText, referralCode, otpCodeString) {

        $.ajax({
            type: "POST",
            url: "https://www.test.jugnoo.in:8012/" + "customer_registeration",
            data: {
                user_name: "",
                email: emailText,
                ph_no: phoneNumber,
                password: passwordText,
                otp: otpCodeString,
                device_type: "2",
                referral_code: referralCode,
                unique_device_id: "webSite",
                device_token: "webSite",
                longitude: "webSite",
                latitude: "webSite",
                country: "webSite",
                device_name: "webSite",
                os_version: "webSite",
                app_version: "webSite"
            },
            success: function (response) {
                console.log(response);
                var responseObject = JSON.parse(response);
                if (responseObject['error']) {
                    $scope.otpMessage = responseObject['error'];
                }
                else {
                    console.log(responseObject);

                    createCookie("access_token", responseObject['user_data']['access_token'], 1);
                    createCookie("user_name", responseObject['user_data']['user_name'], 1);
                    createCookie("user_image", responseObject['user_data']['user_image'], 1);
                    createCookie("phone_no", responseObject['user_data']['phone_no'], 1);
                    createCookie("referral_code", responseObject['user_data']['referral_code'], 1);

                    $window.location.href = '/user_profile';
                }
            }
        });
    }

}]);
app.controller("LoginController", ['$scope', '$location','$route', '$routeParams', function ($scope, $location,$route, $routeParams) {

    $scope.loginEmail = '';
    $scope.loginPassword = '';

    $scope.loginMessage = '';

    $scope.loginClicked = function () {

        var userNameText = $scope.loginEmail;
        var passwordText = $scope.loginPassword;

        if (userNameText.length !== 0 && passwordText.length !== 0) {

            $scope.loginMessage = 'Logging..';
            serverCallToLoginUser(userNameText, passwordText);

        }
        if (userNameText.length == 0) {
            $scope.loginMessage = 'Please fill the email';

        }
        if (passwordText.length == 0) {
            $scope.loginMessage = 'Please fill the password';

        }
    };

    function serverCallToLoginUser(emailText, passwordText) {

        $.ajax({
            type: "POST",
            url: "https://www.dev.jugnoo.in:4012/" + "login_using_email",
            data: {
                client_id: "EEBUOvQq7RRJBxJm",
                email: emailText,
                password: passwordText,
                device_type: "2",
                unique_device_id: "webSite",
                device_token: "webSite",
                longitude: "webSite",
                latitude: "webSite",
                country: "webSite",
                device_name: "webSite",
                os_version: "webSite",
                app_version: "155"
            },
            success: function (response) {
                console.log(response);
                // var responseObject = JSON.parse(response);
                if (response['error']) {

                    if (Number(response['flag']) == 3) {

                        createCookie("otp_email", emailText, 1);

                        createCookie("otp_phone_no", response['phone_no'].toString(), 1);

                        createCookie("otp_password", passwordText, 1);

                        $location.url('/otp');

                        console.log('show OTP');

                    }
                    else {
                        $scope.loginMessage = response['error'];
                        $scope.loginEmail = '';
                        $scope.loginPassword = '';
                        $scope.$apply();

                    }
                }
                else {
                    console.log(response['login']['access_token']);

                    createCookie("access_token", response['login']['access_token'], 1);
                    createCookie("user_name", response['login']['user_name'], 1);
                    createCookie("user_image", response['login']['user_image'], 1);
                    createCookie("phone_no", response['login']['phone_no'], 1);
                    createCookie("referral_code", response['login']['referral_code'], 1);
                    createCookie("current_balance", response['login']['jugnoo_balance'], 1);

                    $location.url('/user_profile');
                    $scope.$apply();
                }
            }
        });
    }
}]);

app.controller("ContactUsController", ["$scope", function ($scope) {

    $scope.successMsg = '';

    /*$scope.$on('mapInitialized', function(evt, evtMap) {
     map = evtMap;
     });*/
    var map;


    var mapOptions = {
        zoom: 18,
        center: new google.maps.LatLng(30.7188978,76.8102981)
    };
    console.log("inside map");

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(30.7188978,76.8102981),
        icon: 'new-website/assets/ContactUs/pin.png',
        map: map
    });

    google.maps.event.addListenerOnce(map, 'idle', function () {
        google.maps.event.trigger(map, 'resize');
        map.setCenter(map.getCenter());
    });

    $("#contact").submit(function (e) {
        /*$scope.sendEmail = function(){*/
        e.preventDefault();
        console.log('sending email');
        var name = $scope.name;
        var email = $scope.email;
        var subject = $scope.subject;
        var message = $scope.message;
        var dataString = 'name=' + name + '&email=' + email + '&subject=' + subject + '&message=' + message;

        function isValidEmail(emailAddress) {
            var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
            return pattern.test(emailAddress);
        }

        if (isValidEmail(email) && (message.length > 1) && (name.length > 1)) {
            console.log('valid data');
            $scope.successMsg = '';
            $.ajax({
                type: "POST",
                url: "new-website/js/mailContact.php",
                data: dataString,
                success: function (response) {
                    console.log(response);
                    console.log("done");
                    $scope.successMsg = 'Email has been sent. Thanks for contacting Jugnoo.';
                    $scope.$apply();
                },
                error: function (response) {
                    console.log(response);
                    console.log("error");
                    $scope.successMsg = 'Email could not be sent. Email ID should be Valid and Message should be longer than 1 characater.';
                    $scope.$apply();

                }

            });

        }
        else {
            $scope.successMsg = 'Email ID should be Valid and Message should be longer than 1 characater.';
            $scope.$apply();
        }

        return false;
    });

}]);